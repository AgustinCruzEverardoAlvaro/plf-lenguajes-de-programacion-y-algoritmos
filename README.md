
# PROGRAMACION LOGICA Y FUNCIONAL -SCC1019-ISA-20213
![](imagenes/tec.png)
## Lenguajes de programación y algoritmos
## Programando ordenadores 80's vs Ahora
### Descripcion
El mapa conceptual explica la manera de programar en años como los 80's o 90's los lenguajes y la forma en que se programaba y de que manera\n.

```plantuml
@startmindmap
<style>
mindmapDiagram{
   
    BackGroundColor  pink
    LineBorder none
    LineColor black
    node{
        LineColor none
        
        BackGroundColor  orange
    }
}
</style>
title  Programando ordenadores 80's vs ahora
*[#green] Programando ordenadores 80's vs ahora
** Antes
***_ son
**** ordenadores de los años 80's y 90's
*****_ se hacian 
****** con diferentes arquitecturas
*******_ pero 
******** no con mas potencia  
***_ funcionaban 
**** como el primer dia y no presentaban actualizaciones
*****_ y 
****** tenian mejor ejecucuion
***_ tenian
****  lenguajes 
*****_ como 
****** ensamblador 
****** c++
****** efield
****** perl 
****** SNOBOL
***_ tenian
**** eficiencia 
*****_ como
****** acceder a la memoria 
****** aprovechar los recursos
****** conocer cada arquitectura
****** conocer los ciclos de ejecución
****_ los 
***** programadores 
******_ tenian
******* mejor entendimiento a ala maquina
******* plantamiento eficiente del problema 
******* conocimiento de la arquitectura
******* codigos optimizados
** ley de moore
***_  es
**** escrita por el ingeniero gordon Moore en 1965\n, cuando era director de los laboratorios Fairchilld Semiconductor 
*****_ trata de 
****** La Ley de Moore por tanto solo nos indica la cantidad de transistores que podemos colocar en un área determinada.

** Ahora
***_ es un
**** sistema actual
*****_ el que 
****** tienes en casa
******  puedes comprar 
******  Google Chrome OS
******  windows 

***_ tiene
**** lenguajes 
*****_ como 
****** java 
*******_ que 
******** compila 
*********_ en una
********** maquina virtual 
***********_ generando 
************  byteCode
****** javaScript
****** c# 
****** PHP 
****** C/C++

***_ tiene
**** claridad
*****_ a la 
****** hora
*******_ de 
******** programar 

***_ puede
**** manejar
*****_ muchas 
****** capas
****** librerias 

***_ tiene
**** codigos menos eficientes 
*****_  y no 
****** conocen el hardware
*****_  y tienen 
****** muchas Actualizaciones 
****** parches y bugs
***_ tienen 
**** software 
*****_ menos 
******  eficiente 
*******_ por 
********  manejar tantas capas 

@endmindmap
```


[¿Qué ha cambiado? (2016)?](https://canal.uned.es/video/5a6f4bdcb1111f082a8b4619)
## Mapa conceptual Hª de los algoritmos y de los lenguajes de programación (2010)
### Descripcion
El mapa conceptual explica la historia de los algoritmos y los lenguajes de programación el tiempo de ejecucion.

```plantuml
@startmindmap
<style>
mindmapDiagram{
   
    BackGroundColor  lightblue
   LineColor  green 
    node{
        BackGroundColor  orange
        lineColor  none 
    }
}
</style>
title  historia de los algoritmos y lenguajes de programacion

* historia de los algoritmos y lenguajes de programacion
** algoritmos
***_  se
**** aplican
*****_ en
****** manuales de usuario 
****** trductores inteligentes 
****** reconocimiento de voz
****** big data
****** automatizacion de procesosor
****** ordenes que recibe el trabajador 
**** secuencia de instrucciones
*****_ son 
******  instrucciones finitas 
****** llevadas a cabo para resolver un problema
****** condicionales 
*******_ como
******** if
******** for
******** if else
******** while
**** tipos
*****_ son 
****** razonables 
 
*******_ es 
******** teimpo de ejecucion crece a medida\n que el problema crece
****** no razonables
*******_ son
******** llamadas
********* exponenciales  
********* super polinomiales 



**  la historia de los lenguajes de\n programación
***_ es
**** primera Generación
*****_ comienza
****** 1950
*******_ con 
******** codigo Maquina
********* el binario: secuencia de unos y ceros.
*********  tarea tediosa y lenta.
********* modificación de los programas resultaba complicada.
********* Solo puede usarse en un tipo de computador
********* explorar al máximo las posibilidades lógicas y la capacidad física del equipo.

**** segunda Generación
*****_ comienza
****** 1955
*******_ con 
******** ensamblador
********* A estos se les denomió lenguaje ensamblador.
*********  usan códigos como a para agregar o mvc para mover, y asi sucesivamente.
********* modificación de los programas resultaba complicada.
********* los programas de utilidad se escriben con frecuencia en un lenguaje ensamblador.
**** Tercera Generación
*****_ año
****** 1957
******* fortran
********_ es
********* FORmula TRANslation
********** lenguje orientado a resolver formulas matematicas
*****_ año
****** 1960
******* COBOL
********_ es
********* lenguaje de gestion
*****_ año
****** 1963
******* PL/I
********_ es un 
********* lenguaje 
*********_ multitarea
*********_  Programacion modular
*****_ año
****** 1964
******* nacen lenguajes sencillos de aprender
********_ es un 
********* lenguaje 
*********_ basic
*********_  logo 
*****_ año
****** 1970
******* pascal
********_ es un 
********* lenguaje 
*********_ estructurado
*********_  Programacion estructurada
****  Cuarta  Generación
*****_ año
****** 1972
******* C
********_ es un 
********* lenguaje 
********** pocas instrucciones pueden traducir cada\n elemento del lenguaje 
*****_ año
****** 1979
******* C++
********_ es un 
********* lenguaje 
********** que su objetivo es extender al lenguaje de programacion C 
*****_ año
****** 1991
******* HTML, PHYTON VISUAL BASIC
********_ es un 
********* lenguaje 
********** ORENTIANDO A OBJETOS 
*****_ año
****** 1995
******* JAVA , JS, PHP
********_ es un 
********* lenguaje 
********** Que se basa definido en la manerar que vemos el mundo


@endmindmap
```
[Los lenguajes de programación (2010)](https://canal.uned.es/video/5a6f4c8fb1111f082a8b4b9c)
## La evolución de los lenguajes y paradigmas de programación (2005)
### Descripcion
El mapa explica los paradigmas de programacion se usaban antes y como fueron evolucionando.

```plantuml
@startmindmap
<style>
mindmapDiagram{
    BackGroundColor  lightblue
    node{
        LineColor none 
        BackGroundColor  yellow
    }
}
</style>
title  Evolucion de los lenguajes y para paradigmas de programacion 

*  Evolucion de los lenguajes y \npara paradigmas de programacion
** paradigma 
***_ es 
**** Como paradigma\n denominamos todo aquel modelo\n, patrón o ejemplo que debe seguirse en determinada situación.
*****_ comenzo por
****** lenguaje ensamblador 
*******_ es
******** paradigma de programación
*********_ como 
********** estructura 
*********** Características 
************ La estructura de los programas es clara
************ Los programas son más fáciles de entender
***********_ aplica 
************ programacion por funciones o rutinas
*************_ aplica 
************** Estructura de control
***************_ Ejemplos
**************** Algol 
**************** Pascal 
**************** Ada
********** Logica 
*********** Características
************  no usan asignación a variables
************  usados en ambientes académicos
***********_ consiste en 
************ Predicado logico 
************  verdadero o falso 
*************_ ejemplos 
************** prolog
************** Lisp
************** Erlang
********** Funcional
*********** categorías 
************ puros
************ híbridos
***********_ utiliza
************ Lenguaje de las matematicas
************_ es 
************* Recursividad
**************_ Ejemplos
*************** Haskel 
*************** OCaml
*************** F#

********** Orientada a Objetos
*********** Funcionalidad
************ Abstracción de datos
************ Encapsulación
************ Eventos
************ Modularidad
************ Herencia
************ Polimorfismo
***********_ es 
************ Abstracción del mundo real 
*************_ Manipulacion \npara obtener 
************** entradas y salidas 
***************_ ejemplos 
**************** Java
**************** JavaScript
**************** PHP


********** atribuida
***********_ es
************ Computadoras conectadas entre si 
*************_ aplican en 
************** Grandes distancias
***************_ ejemplos 
**************** Ada
**************** Alef
**************** Erlang
********** Orientada a aspectos
*********** Ventajas
************ Vuelve más limpio el código fuente
************ Puede mezclarse con cualquier otro paradigma de programación
************ Permite la comunicación entre diferentes lenguajes de programación que comparten aspectos
*********** Desventajas
************ Sufre de un antipatrón de diseño
************ complicado de identificar cuándo es óptimo utilizar POA de forma eficiente
***********_ son
************ Modulos de app
*************_ son
************** Separar Responsabilidades 
***************_ Ejemplo 
**************** Cool
**************** D

********** Orientada a agentes de Software
***********_ es
************ Apps de recomendacion
*************_ Contiene
************** Basada en agentes 
************** sistemas multitangentes
**************_ ejemplos
*************** JavaLog
@endmindmap
```


[Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos](https://canal.uned.es/video/5a6f4d3cb1111f082a8b4fbc)
## Autor
>Everardo Alvaro Agustin Cruz






